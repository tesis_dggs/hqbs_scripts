import numpy as np
import matplotlib.pyplot as plt

import hqbs.hqbs as hq
import hqbs.lattice as l
import hqbs.tessellation as t
import hqbs.hexagonal as h

import snyder.snyder as s

import conversion.conversion as c

print('NIVEL 1')
lps1 = l.LatticePointSystem(radius=0.6954709414939334/1.5*1e16)
print('NIVEL 2')
lps2 = lps1.downLevel()
print('NIVEL 3')
lps3 = lps2.downLevel()
print('NIVEL 4')
lps4 = lps3.downLevel()
print('NIVEL 5')
lps5 = lps4.downLevel()
print('NIVEL 6')
lps6 = lps5.downLevel()
print('NIVEL 7')
lps7 = lps6.downLevel()
# print('NIVEL 8')
# lps8 = lps7.downLevel()
# print('NIVEL 9')
# lps9 = lps8.downLevel()
# print('NIVEL 10')
# lps10 = lps9.downLevel()
# print('NIVEL 11')
# lps11 = lps10.downLevel()
# print('NIVEL 12')
# lps12 = lps11.downLevel()
# print('NIVEL 13')
# lps13 = lps12.downLevel()
# print('NIVEL 14')
# lps14 = lps13.downLevel()
# print('NIVEL 15')
# lps15 = lps14.downLevel()
# print('NIVEL 16')
# lps16 = lps15.downLevel()
# print('NIVEL 17')
# lps17 = lps16.downLevel()
# print('NIVEL 18')
# lps18 = lps17.downLevel()
# print('NIVEL 19')
# lps19 = lps18.downLevel()
# print('NIVEL 20')
# lps20 = lps19.downLevel()
print('CREAR TESSELLATION HEXAGONAL')
h2 = h.HexagonalTessellation(level=2, points=lps2.getTessellationPoints())
h3 = h.HexagonalTessellation(level=3, points=lps3.getTessellationPoints())
h4 = h.HexagonalTessellation(level=4, points=lps4.getTessellationPoints())
h5 = h.HexagonalTessellation(level=5, points=lps5.getTessellationPoints())
h6 = h.HexagonalTessellation(level=6, points=lps6.getTessellationPoints())
h7 = h.HexagonalTessellation(level=7, points=lps7.getTessellationPoints())

lons = np.array([0.95713963, 0.94892172, 0.89472531, 0.73404482, 0.29635216, 0.3908559 , 0.6147094 , 0.49471354, 0.99431917, 0.53393867, 0.3248369 , 0.68774646, 0.9406557 , 0.8874145 , 0.03220679, 0.38677154, 0.54519198, 0.07044661, 0.42296684, 0.75286468]) * 72 - 36
lats = np.array([0.38690183, 0.92664222, 0.11898785, 0.1501156 , 0.13404069, 0.37111644, 0.79894408, 0.57919471, 0.0117501 , 0.51405181, 0.98942251, 0.56562294, 0.89738355, 0.15747138, 0.14077632, 0.24431954, 0.34245492, 0.90657668, 0.20948794, 0.79377719]) * 55 + 30

proj = s.ISEA(ajuste=1e16)

nivel = 2

print('PUNTOS')
if (nivel == 2):
    for i in range(len(lons)):
        h2.draw('c')
        (x, y, cara) = proj.forwardProjection(dlon=lons.item(i), dlat=lats.item(i))
        plt.scatter(x, y, c='#000000')
        (f, g) = h2.getCode(lon=lons.item(i), lat=lats.item(i))
        plt.title('Tessel: {}\n lon = {} - lat = {}'.format(g, lons.item(i), lats.item(i)))
        plt.axis('off')
        plt.show()
elif (nivel == 3):
    for i in range(len(lons)):
        h3.draw('c')
        (x, y, cara) = proj.forwardProjection(dlon=lons.item(i), dlat=lats.item(i))
        plt.scatter(x, y, c='#000000')
        (f, g) = h3.getCode(lon=lons.item(i), lat=lats.item(i))
        plt.title('Tessel: {}\n lon = {} - lat = {}'.format(g, lons.item(i), lats.item(i)))
        plt.axis('off')
        plt.show()
elif (nivel == 4):
    for i in range(len(lons)):
        h4.draw('c')
        (x, y, cara) = proj.forwardProjection(dlon=lons.item(i), dlat=lats.item(i))
        plt.scatter(x, y, c='#000000')
        (f, g) = h4.getCode(lon=lons.item(i), lat=lats.item(i))
        plt.title('Tessel: {}\n lon = {} - lat = {}'.format(g, lons.item(i), lats.item(i)))
        plt.axis('off')
        plt.show()
elif (nivel == 5):
    for i in range(len(lons)):
        h5.draw('c')
        (x, y, cara) = proj.forwardProjection(dlon=lons.item(i), dlat=lats.item(i))
        plt.scatter(x, y, c='#000000')
        (f, g) = h5.getCode(lon=lons.item(i), lat=lats.item(i))
        plt.title('Tessel: {}\n lon = {} - lat = {}'.format(g, lons.item(i), lats.item(i)))
        plt.axis('off')
        plt.show()
elif (nivel == 6):
    for i in range(len(lons)):
        h6.draw('c')
        (x, y, cara) = proj.forwardProjection(dlon=lons.item(i), dlat=lats.item(i))
        plt.scatter(x, y, c='#000000')
        (f, g) = h6.getCode(lon=lons.item(i), lat=lats.item(i))
        plt.title('Tessel: {}\n lon = {} - lat = {}'.format(g, lons.item(i), lats.item(i)))
        plt.axis('off')
        plt.show()
elif (nivel == 7):
    for i in range(len(lons)):
        h7.draw('c')
        (x, y, cara) = proj.forwardProjection(dlon=lons.item(i), dlat=lats.item(i))
        plt.scatter(x, y, c='#000000')
        (f, g) = h7.getCode(lon=lons.item(i), lat=lats.item(i))
        plt.title('Tessel: {}\n lon = {} - lat = {}'.format(g, lons.item(i), lats.item(i)))
        plt.axis('off')
        plt.show()
