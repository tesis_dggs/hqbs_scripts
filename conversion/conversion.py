#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 12:18:16 2019

@author: federico
"""

import sys
sys.path.append("../")
from snyder.snyder import ISEA, FullersDymaxion
from hqbs.hqbs import Code
from hqbs.hqbs import Sequence
import numpy as np
from coordinates.coordinates import WGS84Coordinate, CartesianCoordinate, TiltedCoordinate, TiltedLatticePointCoordinate, HQBSCoordinate

from matplotlib import pyplot as plt
from decimal import Decimal

class Converter:
    
    def __init__(self, dist, level):
        self._level = level
        self._dist = Decimal(dist)
        self._axe = np.array([np.cos(2*np.pi/3), np.sin(2*np.pi/3)])
        self._proj = ISEA(ajuste=1e16)

        # CONSTANTES
        self._invC2 = np.matrix([[3, np.sqrt(3)],[0, 2*np.sqrt(3)]])/3
        self._invC1 = np.matrix([[-1, 2],[-2, 1]])
        self._C2 = np.matrix([[2, -1],[0, np.sqrt(3)]])/2

    def _toTiltedCoordinateSystem(self, cartesianCoord):
        coord = np.array([[cartesianCoord.x],[cartesianCoord.y]])
        IJ = self._invC2*coord
        return TiltedCoordinate(I=IJ.item(0, 0), J=IJ.item(1, 0))

    def _calculateAngle(self, coord, center):
        dI = coord.I - center.I
        dJ = coord.J - center.J
        Isquare = dI**2
        Jsquare = dJ**2
        cosineFactor = Decimal(2.0) * dI * dJ * Decimal(np.cos(2.0 * np.pi / 3.0))
        d = Decimal(np.sqrt(Isquare + Jsquare + cosineFactor))
        if (coord.I == 0):
            gamma = Decimal(2.0*np.pi/3.0)
        else:
            f = (d**2 + dI**2 - dJ**2) / (2 * d * dI)
            gamma = Decimal(np.arccos(float(f)))
        n = gamma // Decimal(np.pi / 6.0)
        if (n % 2 != 0):
            n = n + 1
        gamma = gamma - Decimal(np.pi) * n / Decimal(6.0)
        return (gamma, d)
    
    def _isOutHexagon(self, angle, distance):
        m = self._dist / Decimal(2.0 * np.cos(float(angle)))
        return distance > m
    
    def _centerTiltedCoordinates(self, tiltedCoord):
        ddI = Decimal(tiltedCoord.I)
        ddJ = Decimal(tiltedCoord.J)
        # centrar I
        sI = np.sign(ddI)
        nI = int(np.abs(ddI) // self._dist)
        rI = np.abs(ddI) % self._dist
        if (rI < self._dist / Decimal(2.0)):
            I = sI * nI * self._dist
        else:
            I = sI * (nI + 1) * self._dist
        # centrar J
        sJ = np.sign(ddJ)
        nJ = int(np.abs(ddJ) // self._dist)
        rJ = np.abs(ddJ) % self._dist
        if (rJ < self._dist / Decimal(2.0)):
            J = sJ * nJ * self._dist
        else:
            J = sJ * (nJ + 1) * self._dist

        # (xx, yy) = self._volverAxy(I=float(I), J=float(J))
        # (xxx, yyy) = self._volverAxy(I=dI, J=dJ)
        # plt.scatter(xx, yy, c='#0000ff', marker='*')
        # plt.scatter(xxx, yyy, c='#000000')
        # plt.show()

        # calcular angulo gamma
        (gamma, d) = self._calculateAngle(coord=tiltedCoord, center=TiltedCoordinate(I=I, J=J))
        deltaI = np.abs(ddI - I)
        deltaJ = np.abs(ddJ - J)
        if (self._isOutHexagon(angle=gamma, distance=d)):
            if (deltaI > deltaJ):
                I += np.sign(ddI - I) * self._dist
            else:
                J += np.sign(ddJ - J) * self._dist
        return TiltedCoordinate(I=float(I), J=float(J))

    def _toTiltedLatticePointCoordinateSystem(self, tiltedCoord):
        print('I: {}, J: {}, dist: {}'.format(tiltedCoord.I, tiltedCoord.J, self._dist))
        coordenada = np.array([[Decimal(tiltedCoord.I) // self._dist],[Decimal(tiltedCoord.J) // self._dist]])
        print('coord: {}'.format(coordenada))
        ij = self._invC1*coordenada
        return TiltedLatticePointCoordinate(i=int(ij.item(0, 0)), j=int(ij.item(1, 0)))

    def _toCodes(self, tiltedLatticeCoord):
        v1 = [0] * self._level
        v2 = [0] * self._level
        v1[self._level - 1] = np.sign(tiltedLatticeCoord.i) * (-1)**(self._level+1)
        v2[self._level - 1] = np.sign(tiltedLatticeCoord.j) * (-1)**(self._level+1) * 2
        # if (i < 0):
        #     v1[self._level-1] = -1
        # else:
        #     v1[self._level-1] = 1
        # if (j < 0):
        #     v2[self._level-1] = -2
        # else:
        #     v2[self._level-1] = 2
        i = np.abs(tiltedLatticeCoord.i)
        j = np.abs(tiltedLatticeCoord.j)
        s1 = Sequence(level=self._level, c=v1)
        s2 = Sequence(level=self._level, c=v2)
        aux1 = Sequence(level=self._level)
        for _ in range(np.abs(tiltedLatticeCoord.i)):
            aux1 += s1
        # aux1 *= s1
        aux2 = Sequence(level=self._level)
        for _ in range(np.abs(tiltedLatticeCoord.j)):
            aux2 += s2
        # aux2 *= s2
        c = aux1 + aux2
        return Code(sequence=c)

    def _volverAxy(self, I, J):
        coord = np.array([[I],[J]])
        xy = self._C2*coord
        return (xy.item(0, 0), xy.item(1, 0))

    def WGS842HQBS(self, coord):
        # (lat,lon) -Snyder Proj-> (u,v)
        (cartesianCoord, cara) = self._proj.forwardProjection(coord=coord)
        # (x,y) -Tilted Coordinate System-> (dI,dJ)
        tiltedCoord = self._toTiltedCoordinateSystem(cartesianCoord=cartesianCoord)
        # (dI,dJ) -escalamiento-> (I,J)
        centerTiltedCoord = self._centerTiltedCoordinates(tiltedCoord=tiltedCoord)
        # (I,J) -Tilted Lattice Point Coordinate System-> (i,j)
        tiltedLatticeCoord = self._toTiltedLatticePointCoordinateSystem(tiltedCoord=centerTiltedCoord)
        # (i,j) -Codes-> G
        G = self._toCodes(tiltedLatticeCoord=tiltedLatticeCoord)
        (xx, yy) = self._volverAxy(I=centerTiltedCoord.I, J=centerTiltedCoord.J)
        plt.scatter(xx, yy, c='#0000ff', marker='*')
        return HQBSCoordinate(face=cara, code=G)

class FullersDymaxionConverter(Converter):

    def __init__(self, dist, level):
        super().__init__(dist=dist, level=level)
        self._proj = FullersDymaxion(ajuste=1e16)
