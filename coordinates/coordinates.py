import numpy as np

class WGS84Coordinate:
    def __init__(self, longitude, latitude):
        self.lat = latitude
        self.lon = longitude
    
    def __str__(self):
        ns = 'N' if self.lat >= 0.0 else 'S'
        eo = 'E' if self.lon >= 0.0 else 'W'
        return "longitud {} {}, latitud {} {}".format(np.abs(self.lon), eo, np.abs(self.lat), ns)

class CartesianCoordinate:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class TiltedCoordinate:
    def __init__(self, I, J):
        self.I = I
        self.J = J

class TiltedLatticePointCoordinate:
    def __init__(self, i, j):
        self.i = i
        self.j = j

class HQBSCoordinate:
    def __init__(self, code, face):
        self.code = code
        self.face = face