#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 12:56:47 2019

@author: federico
"""
from hqbs.lattice import LatticePoint

class Tessel(LatticePoint):
    def __init__(self, radius, centroid, code):
        self._radius = radius
        self._centroid = centroid
        self._code = code

    def getGeometryDistance(self):
        pass
    
    def draw(self, color):
        pass

class Tessellation:
    def __init__(self, level, tessels):
        self._level = level
        self._tessels = tessels

    def draw(self, color):
        for tessel in self._tessels:
            tessel.draw(color)

    # depende de la geometria del tessel
    def getCode(self, lon, lat):
        pass
