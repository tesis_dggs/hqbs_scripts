#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 15:34:47 2019

@author: federico
"""

import numpy as np
import matplotlib.pyplot as plt
from hqbs.hqbs import Sequence

def _pol2cart(theta, rho):
    x = rho*np.cos(theta)
    y = rho*np.sin(theta)
    return (x, y)

class LatticePoint:
    def __init__(self, radius, centroid, sequence):
        self._radius = radius
        self._centroid = centroid
        self._sequence = sequence
        
    def side(self):
        return 2*self._radius*np.sin(np.pi/6)
    
    def apothem(self):
        return self._radius*np.cos(np.pi/6)
    
    def radius(self):
        return self._radius
    
    def setRadius(self, r):
        self._radius = r
    
    def centroid(self):
        return self._centroid
    
    def sequence(self):
        return self._sequence
    
    def aperture4HIII(self, sign):
#        r = [LatticePoint(radius=self._radius/2, centroid=self._centroid, sequence=self._sequence*Sequence(c=[1,0]))]
        r = []
        rho = self._radius/2. * np.ones(3)
        theta = np.array([np.pi/2., 7.*np.pi/6., 11.*np.pi/6.])
        (x, y) = _pol2cart(theta, rho)
        c = self._sequence*Sequence(c=[1,0])
        for i in range(0, 3):
            #c = self._sequence
            r.append(LatticePoint(radius=self._radius/2., centroid=np.array([x[i], y[i]])+self._centroid, sequence=c+Sequence(c=[sign*(i+1)])))
        return r
    
    def inverseAperture4HIII(self, sign):
#        r = [LatticePoint(radius=self._radius/2, centroid=self._centroid, sequence=self._sequence*Sequence(c=[1,0]))]
        r = []
        rho = self._radius/2. * np.ones(3)
        theta = np.array([3.*np.pi/2., np.pi/6., 5.*np.pi/6.])
        (x, y) = _pol2cart(theta, rho)
        c = self._sequence*Sequence(c=[1,0])
        for i in range(0, 3):
            #c = self._sequence
            r.append(LatticePoint(radius=self._radius/2., centroid=np.array([x[i], y[i]])+self._centroid, sequence=c+Sequence(c=[-sign*(i+1)])))
        return r
        
class LatticePointSystem:
    def __init__(self, level=None, t4hi=None, t4hiii=None, radius=None):
        self._t4HI = t4hi
        self._t4HIII = t4hiii
        if level is None:
            level = 1
            if radius is None:
                radius = 1
            p = LatticePoint(radius=2.0*radius, centroid=np.array([0,0]), sequence=Sequence(c=[0]))
            self._t4HIII = p.aperture4HIII(sign=1)
            p.setRadius(r=radius)
            self._t4HI = [p]
        self._level = level
        
    def downLevel(self):
        nivel = self._level + 1
        remainder = nivel % 2
#        t4hi = copy.deepcopy(self._t4HI)
#        for tessel in t4hi:
#            tessel.setRadius(tessel.radius()/2.)
#        t4hiii = copy.deepcopy(self._t4HIII)
#        for tessel in t4hiii:
#            tessel.setRadius(tessel.radius()/2.)
        finalT4hiii = []
        finalT4hi = []
        if (remainder == 1):
            for tessel in self._t4HI:
                finalT4hiii += tessel.aperture4HIII(sign=1)
            for tessel in self._t4HIII:
                finalT4hi += tessel.inverseAperture4HIII(sign=1)
        else:
            for tessel in self._t4HI:
                finalT4hiii += tessel.inverseAperture4HIII(sign=-1)
            for tessel in self._t4HIII:
                finalT4hi += tessel.aperture4HIII(sign=-1)
        initialT4hi = []
        initialT4hiii = []
        for tessel in self._t4HI:
            initialT4hi.append(LatticePoint(radius=tessel.radius()/2.,centroid=tessel.centroid(), sequence=tessel.sequence()*Sequence(c=[1,0])))
        for tessel in self._t4HIII:
            initialT4hiii.append(LatticePoint(radius=tessel.radius()/2.,centroid=tessel.centroid(), sequence=tessel.sequence()*Sequence(c=[1,0])))
        t = LatticePointSystem(level=nivel, t4hi=initialT4hi+finalT4hi, t4hiii=initialT4hiii+finalT4hiii)
        return t
    
    def getTessellationPoints(self):
        return self._t4HI
    
    def draw(self):
        for t in self._t4HI:
            plt.scatter(t.centroid()[0],t.centroid()[1],c='b')
            plt.text(t.centroid()[0], t.centroid()[1]+.1, str(t.sequence()), horizontalalignment='center', color='b')
        for t in self._t4HIII:
            plt.scatter(t.centroid()[0],t.centroid()[1],c='r')
            plt.text(t.centroid()[0], t.centroid()[1]+.1, str(t.sequence()), horizontalalignment='center', color='r')
        plt.show()

class HQBSTree:
    def __init__(self, levels):
        self._colours = {1: 'b', 2: 'r', 3: 'g', 4: 'y', 5: 'm'}
        self.root = {}
        self.root['point'] = LatticePoint(radius=1, centroid=np.array([0,0]), sequence=Sequence(c=[0]))
        self.root['level'] = 1
        self.root['invert'] = True
        self.root['childrens'] = []
        childPoints = self.root['point'].aperture4HIII()
        for child in childPoints:
            newNode = {}
            newNode['point'] = child
            newNode['level'] = 1
            newNode['invert'] = False
            newNode['childrens'] = []
            self.root['childrens'].append(newNode)
        for i in range(2,levels+1):
            self.appendLevel(self.root)
        
    def appendLevel(self, node):
        for child in node['childrens']:
            self.appendLevel(child)
        p = node['point']
        newChildPoints = []
        if node['invert']:
            newChildPoints = p.inverseAperture4HIII()
        else:
            newChildPoints = p.aperture4HIII()
        for newChild in newChildPoints:
            newChild.setRadius(1./2**(node['level']+1))
            newNode = {}
            newNode['point'] = newChild
            newNode['level'] = node['level'] + 1
            newNode['invert'] = False
            newNode['childrens'] = []
            node['childrens'].append(newNode)
        node['invert'] = not node['invert']
        
    def drawPoints(self, node):
        p = node['point'].centroid()
        l = node['level']
        plt.scatter(p[0], p[1], s=1000/l, c=self._colours[l])
        for child in node['childrens']:
            self.drawPoints(child)
    
    def draw(self):
        self.drawPoints(self.root)
    
#g1 = LatticePointSystem()
#g2 = g1.downLevel()