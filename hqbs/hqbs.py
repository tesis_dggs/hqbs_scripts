#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 12:25:32 2019

@author: federico
"""
import numpy as np

AddLookupTable = {
        (-3,-3):( 3, 0), (-3,-2):( 0, 1), (-3,-1):( 0, 2), (-3, 0):( 0,-3), (-3, 1):( 3,-2), (-3, 2):( 3,-1), (-3, 3):( 0, 0),
        (-2,-3):( 0, 1), (-2,-2):( 2, 0), (-2,-1):( 0, 3), (-2, 0):( 0,-2), (-2, 1):( 2,-3), (-2, 2):( 0, 0), (-2, 3):( 2,-1),
        (-1,-3):( 0, 2), (-1,-2):( 0, 3), (-1,-1):( 1, 0), (-1, 0):( 0,-1), (-1, 1):( 0, 0), (-1, 2):( 1,-3), (-1, 3):( 1,-2),
        ( 0,-3):( 0,-3), ( 0,-2):( 0,-2), ( 0,-1):( 0,-1), ( 0, 0):( 0, 0), ( 0, 1):( 0, 1), ( 0, 2):( 0, 2), ( 0, 3):( 0, 3),
        ( 1,-3):( 3,-2), ( 1,-2):( 2,-3), ( 1,-1):( 0, 0), ( 1, 0):( 0, 1), ( 1, 1):(-1, 0), ( 1, 2):( 0,-3), ( 1, 3):( 0,-2),
        ( 2,-3):( 3,-1), ( 2,-2):( 0, 0), ( 2,-1):( 1,-3), ( 2, 0):( 0, 2), ( 2, 1):( 0,-3), ( 2, 2):(-2, 0), ( 2, 3):( 0,-1),
        ( 3,-3):( 0, 0), ( 3,-2):( 2,-1), ( 3,-1):( 1,-2), ( 3, 0):( 0, 3), ( 3, 1):( 0,-2), ( 3, 2):( 0,-1), ( 3, 3):(-3, 0)
        }
MulLookupTable = {
        (-3,-3): 2, (-3,-2): 1, (-3,-1): 3, (-3, 0): 0, (-3, 1):-3, (-3, 2):-1, (-3, 3):-2,
        (-2,-3): 1, (-2,-2): 3, (-2,-1): 2, (-2, 0): 0, (-2, 1):-2, (-2, 2):-3, (-2, 3):-1,
        (-1,-3): 3, (-1,-2): 2, (-1,-1): 1, (-1, 0): 0, (-1, 1):-1, (-1, 2):-2, (-1, 3):-3,
        ( 0,-3): 0, ( 0,-2): 0, ( 0,-1): 0, ( 0, 0): 0, ( 0, 1): 0, ( 0, 2): 0, ( 0, 3): 0,
        ( 1,-3):-3, ( 1,-2):-2, ( 1,-1):-1, ( 1, 0): 0, ( 1, 1): 1, ( 1, 2): 2, ( 1, 3): 3,
        ( 2,-3):-1, ( 2,-2):-3, ( 2,-1):-2, ( 2, 0): 0, ( 2, 1): 2, ( 2, 2): 3, ( 2, 3): 1,
        ( 3,-3):-2, ( 3,-2):-1, ( 3,-1):-3, ( 3, 0): 0, ( 3, 1): 3, ( 3, 2): 1, ( 3, 3): 2
        }

class Sequence:
    def __init__(self, level=None, c=None):
        if level is None:
            if c is None:
                self._level = None
                self._code = None
            else:
                self._level = len(c)
                self._code = np.flip(c)
        else:
            if c is None:
                self._level = level
                self._code = np.zeros(self._level, dtype=int)
            else:
                if level == len(c):
                    self._level = level
                    self._code = np.flip(c)
                else:
                    raise Exception('nivel y longitud del codigo deben coincidir: {} != {}'.format(level, len(c)))
    
    def __getitem__(self, arg):
        if arg >= len(self._code):
            raise IndexError()
        if arg < 0:
            raise IndexError()
        return self._code[arg]
    
    def __setitem__(self, key, item):
        if key >= len(self._code):
            raise IndexError()
        if key < 0:
            raise IndexError()
        self._code[key] = item
        
    def __str__(self):
        return '(' + ','.join(str(a) for a in np.flip(self._code)) + ')'
    
    def __eq__(self, other):
        return (self._level == other._level) and np.array_equal(self._code,other._code)
    
    def __ne__(self, other):
        return not (self == other)
    
    def __mul__(self, other):
        mu = self._level
        lam = other._level
        if (lam < mu):
            return other * self
        i = 0
        L = Sequence(level=1)
        G = other
        H = self
        while (i < mu):
            j = 0
            M = Sequence(level=lam+i)
            while (j < lam):
                M[j+i] = MulLookupTable[G[j],H[i]]
                j += 1
            else:
                L = L+M
                i += 1
        return L
    
    def __invert__(self):
        self._code = -1*self._code
        return self
    
    def __add__(self, other):
        G = other
        H = self
        mu = self._level
        lam = other._level
        if (lam < mu):
            return other + self
        j = 0
        i = 0
        L = Sequence(level=lam+1)
        #step 2
        while (i < mu):
            k = AddLookupTable[G[i],H[i]]
            m = AddLookupTable[k[1],j]
            L[i] = m[1]
            j = AddLookupTable[k[0],m[0]][1]
            i += 1
        else:
            #step 3
            while (i < lam):
                #step 4
                if (j == 0):
                    L[i] = G[i]
                    #go step 3
                else:
                    k = AddLookupTable[G[i],j]
                    L[i] = k[1]
                    j = k[0]
                    #go step 3
                i += 1
            else:
                #step 5
                L[i] = j
                L._code = np.trim_zeros(L._code, 'b')
                if (len(L._code) == 0):
                    L._code = np.array([0])
                L._level = len(L._code)
        return L
    
    def _f1(self, code):
        return -1 if code == 0 else 1
    
class Code(Sequence):
    def __init__(self, level=None, c=None, sequence=None):
        if sequence is None:
            if level is None:
                if c is None:
                    self._legalLevel = None
                    self._legalCode = None
                    Sequence.__init__(self)
                else:
                    self._legalLevel = len(c)
                    self._legalCode = np.flip(c)
                    Sequence.__init__(self, c=np.flip(self._expand()))
            else:
                if c is None:
                    self._legalLevel = level
                    self._legalCode = np.zeros(self._level, dtype=int)
                    Sequence.__init__(self, level=level)
                else:
                    if level == len(c):
                        self._legalLevel = level
                        self._legalCode = np.flip(c)
                        Sequence.__init__(self, c=np.flip(self._expand()))
                    else:
                        raise Exception('nivel y longitud del codigo deben coincidir: {} != {}'.format(level, len(c)))
        else:
            self._code = sequence._code
            self._level = sequence._level
            self._legalCode = self._normalize()
            self._code = sequence._code
            self._legalLevel = len(self._legalCode)
    
    def __getitem__(self, arg):
        if arg >= len(self._legalCode):
            raise IndexError()
        if arg < 0:
            raise IndexError()
        return self._legalCode[arg]
    
    def __setitem__(self, key, item):
        if key >= len(self._legalCode):
            raise IndexError()
        if key < 0:
            raise IndexError()
        self._legalCode[key] = item
        
    def __str__(self):
        return '(' + ','.join(str(a) for a in np.flip(self._legalCode)) + ')'
    
    def __eq__(self, other):
        return (self._legalLevel == other._legalLevel) and np.array_equal(self._legalCode,other._legalCode)
    
    def __ne__(self, other):
        return not (self == other)
    
    def _expand(self):
        sequence = np.zeros(self._legalLevel, dtype=int)
        for i in range(self._legalLevel):
            d = (-1)**i
            for j in range(self._legalLevel-i-1, self._legalLevel):
                d *= self._f1(self[j])
            sequence[self._legalLevel-i-1] = d*self[self._legalLevel-i-1]
        return sequence

    def __add__(self, other):
        L = Sequence.__add__(Sequence(c=np.flip(self._code)), Sequence(c=np.flip(other._code)))
        return Code(sequence=L)
    
    def __invert__(self):
        L = Sequence.__invert__(self)
        return Code(sequence=L)
    
    def __mul__(self, other):
        L = Sequence.__mul__(Sequence(c=np.flip(self._code)), Sequence(c=np.flip(other._code)))
        return Code(sequence=L)
    
    def _normalize(self):
        code = self
        if ((code._code < 0).sum() == code._level):
            code._code = -1*code._code
            codigo = code._normalize()
            code = Code(c=np.flip(codigo))
            code = ~code
        G = np.zeros(code._level+1, dtype=int)
        A = np.append(code._code,0)
        #A[0] = np.abs(A[0])
        n = code._level
        i = 0
        #setp 2
        while (i <= n-1):
            #step 3
            if (A[i] != 0):
                i_inicial = i
                l = -1
                #step 4
                while (l <= 1):
                    G[i] = l*A[i]
                    j = 0 if l > 0 else G[i]
                    f = -1 if G[i] > 0 else 1
                    #step 5
                    while (i < n):
                        k = AddLookupTable[A[i+1],j]
                        if (k[1] == 0):
                            G[i+1] = 0
                            j = k[0]
                            #go step 5
                        else:
                            #step 7
                            if (k[1]*f > 0):
                                G[i+1] = k[1]
                                j = k[0]
                                #go step 5
                            else:
                                #step 8
                                G[i+1] = -k[1]
                                j = AddLookupTable[G[i+1],k[0]][1]
                            f = -f
                            #go step 5
                        i += 1
                    else:
                        #step 9
                        if (j == 0):
                            z = n
                            #step 10
                            while (G[z] == 0):
                                z -= 1
                            #step 11 buscar el ultimo negativo
                            if (G[z] < 0):
                                l += 2
                                i = i_inicial
                                #i = min(np.where(G<0)[0])
                                #i = z-1
                                #go step 4
                            else:
                                #step 13
                                for m in range(len(G)): G[m] = np.abs(G[m])
                                G = np.trim_zeros(G, 'b')
                                if (len(G) == 0):
                                    G = np.array([0])
                                return G
                        else:
                            #step 12
                            if ((j < 0) or (f*j < 0)):
                                l += 2
                                #go step 4
                            else:
                                #G[n] = j
                                #go step 13
                                for m in range(len(G)): G[m] = np.abs(G[m])
                                G = np.trim_zeros(G, 'b')
                                if (len(G) == 0):
                                    G = np.array([0])
                                return G
                else:
                    return None
            else:
                G[i] = 0
                i += 1
        for m in range(len(G)): G[m] = np.abs(G[m])
        G = np.trim_zeros(G, 'b')
        if (len(G) == 0):
            G = np.array([0])
        return G
