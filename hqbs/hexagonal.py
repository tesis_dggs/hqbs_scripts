#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 13:16:34 2019

@author: federico
"""

import numpy as np
import matplotlib.pyplot as plt
from hqbs.tessellation import Tessel, Tessellation
from hqbs.hqbs import Code
from conversion.conversion import Converter

class HexagonalTessel(Tessel):
    def draw(self, color):
        t = np.arange(np.pi/6,(2*np.pi+np.pi/3),2*np.pi/6)
        x = self._radius*np.cos(t) + self._centroid[0]
        y = self._radius*np.sin(t) + self._centroid[1]
        plt.plot(x, y, color)
        plt.text(self._centroid[0], self._centroid[1], str(self._code), horizontalalignment='center', color=color)

    def getGeometryDistance(self):
        return np.sqrt(3) * self._radius

class HexagonalTessellation(Tessellation):
    def __init__(self, level, points):
        self._level = level
        self._tessels = []
        for point in points:
            #print(point.sequence())
            self._tessels.append(HexagonalTessel(radius=point.radius(), centroid=point.centroid(), code=Code(sequence=point.sequence())))
            #self._tessels.append(HexagonalTessel(radius=point.radius(), centroid=point.centroid(), code=point.sequence()))
        self._converter = Converter(dist=self._tessels[0].getGeometryDistance(), level=self._level)
            
    def draw(self, color):
        Tessellation.draw(self, color)
        # plt.show()

    def getCode(self, lon, lat):
        return self._converter.WGS842HQBS(lon=lon, lat=lat)