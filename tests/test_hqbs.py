#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 16:07:31 2019

@author: federico
"""
import unittest
import sys
sys.path.append("../")
from hqbs.hqbs import Code
from hqbs.hqbs import Sequence
import numpy as np

class TestCase(unittest.TestCase):
    def setUp(self):
        self.code11 = Code(c=[1,1])
        self.code12 = Code(c=[1,2])
        self.code13 = Code(c=[1,3])
        self.code21 = Code(c=[2,1])
        self.code22 = Code(c=[2,2])
        self.code23 = Code(c=[2,3])
        self.code31 = Code(c=[3,1])
        self.code32 = Code(c=[3,2])
        self.code33 = Code(c=[3,3])
        self.code101 = Code(c=[1,0,1])
        self.code103 = Code(c=[1,0,3])
        self.code120 = Code(c=[1,2,0])
        self.code201 = Code(c=[2,0,1])
        self.code202 = Code(c=[2,0,2])
        self.code230 = Code(c=[2,3,0])
        self.code303 = Code(c=[3,0,3])
    
    def test_equals(self):
        codeA = Code(c=[2,0,2])
        codeB = Code(c=[2,1,0])
        codeC = Code(c=[2,0,2])
        self.assertTrue(codeA == codeC, 'operador == no funciona')
        self.assertFalse(codeA == codeB, 'operador == no funciona')
        self.assertTrue(codeA != codeB, 'operador != no funciona')
        self.assertFalse(codeA != codeC, 'operador != no funciona')
    
    def test_normalization(self):
        c22 = Code(sequence=Sequence(c=[-2,0,2]))
        c21 = Code(sequence=Sequence(c=[3,3,1]))
        c303 = Code(sequence=Sequence(c=[-3,3]))
        c11 = Code(sequence=Sequence(c=[-1,0,1]))
        c13 = Code(sequence=Sequence(c=[-1,-1,-3]))
        c101 = Code(sequence=Sequence(c=[-1,1]))
        c31 = Code(sequence=Sequence(c=[2,2,1]))
        c201 = Code(sequence=Sequence(c=[2,1,1]))
        c32 = Code(sequence=Sequence(c=[-3,-3,-2]))
        c33 = Code(sequence=Sequence(c=[-3,-3,-3]))
        b33 = Code(sequence=Sequence(c=[-3,0,3]))
        b303 = Code(sequence=Sequence(c=[3,3,3]))
        n22 = c22._normalize()
        n21 = c21._normalize()
        n303 = c303._normalize()
        n11 = c11._normalize()
        n13 = c13._normalize()
        n101 = c101._normalize()
        n31 = c31._normalize()
        n201 = c201._normalize()
        n32 = c32._normalize()
        n33 = c33._normalize()
        m33 = b33._normalize()
        m303 = b303._normalize()
        self.assertIsNotNone(n22, 'no se pudo normalizar {}'.format(c22))
        self.assertIsNotNone(n21, 'no se pudo normalizar {}'.format(c21))
        self.assertIsNotNone(n303, 'no se pudo normalizar {}'.format(c303))
        self.assertIsNotNone(n11, 'no se pudo normalizar {}'.format(c11))
        self.assertIsNotNone(n13, 'no se pudo normalizar {}'.format(c13))
        self.assertIsNotNone(n101, 'no se pudo normalizar {}'.format(c101))
        self.assertIsNotNone(n31, 'no se pudo normalizar {}'.format(c31))
        self.assertIsNotNone(n201, 'no se pudo normalizar {}'.format(c201))
        self.assertIsNotNone(n32, 'no se pudo normalizar {}'.format(c32))
        self.assertIsNotNone(n33, 'no se pudo normalizar {}'.format(c33))
        self.assertIsNotNone(m33, 'no se pudo normalizar {}'.format(b33))
        self.assertIsNotNone(m303, 'no se pudo normalizar {}'.format(b303))
        self.assertTrue(np.array_equal(n22, np.array([2,2])), 'error en la normalizacion {} != {}'.format(n22, self.code22))
        self.assertTrue(np.array_equal(n21, np.array([1,2])), 'error en la normalizacion {} != {}'.format(n21, self.code21))
        self.assertTrue(np.array_equal(n303, np.array([3,0,3])), 'error en la normalizacion {} != {}'.format(n303, self.code303))
        self.assertTrue(np.array_equal(n11, np.array([1,1])), 'error en la normalizacion {} != {}'.format(n11, self.code11))
        self.assertTrue(np.array_equal(n13, np.array([3,1])), 'error en la normalizacion {} != {}'.format(n13, self.code13))
        self.assertTrue(np.array_equal(n101, np.array([1,0,1])), 'error en la normalizacion {} != {}'.format(n101, self.code101))
        self.assertTrue(np.array_equal(n31, np.array([1,3])), 'error en la normalizacion {} != {}'.format(n31, self.code31))
        self.assertTrue(np.array_equal(n201, np.array([1,0,2])), 'error en la normalizacion {} != {}'.format(n201, self.code201))
        self.assertTrue(np.array_equal(n32, np.array([2,3])), 'error en la normalizacion {} != {}'.format(n32, self.code32))
        self.assertTrue(np.array_equal(n33, np.array([3,3])), 'error en la normalizacion {} != {}'.format(c33._normalize(), self.code33))
        self.assertTrue(np.array_equal(m33, np.array([3,3])), 'error en la normalizacion {} != {}'.format(m33, self.code33))
        self.assertTrue(np.array_equal(m303, np.array([3,0,3])), 'error en la normalizacion {} != {}'.format(m303, self.code303))

    def test_sum(self):
        s22 = self.code23+self.code21
        s21 = self.code303+self.code32
        s303 = self.code21+self.code12
        s11 = self.code13+self.code12
        s13 = self.code230+self.code23
        s101 = self.code23+self.code32
        s31 = self.code202+self.code23
        s201 = self.code202+self.code13
        s32 = self.code120+self.code12
        s33 = self.code103+self.code230
        self.assertEqual(s22, self.code22, '{} + {} != {}'.format(self.code23, self.code21, s22))
        self.assertEqual(s21, self.code21, '{} + {} != {}'.format(self.code303, self.code32, s22))
        self.assertEqual(s303, self.code303, '{} + {} != {}'.format(self.code21, self.code12, s22))
        self.assertEqual(s11, self.code11, '{} + {} != {}'.format(self.code13, self.code12, s22))
        self.assertEqual(s13, self.code13, '{} + {} != {}'.format(self.code230, self.code23, s22))
        self.assertEqual(s101, self.code101, '{} + {} != {}'.format(self.code23, self.code32, s22))
        self.assertEqual(s31, self.code31, '{} + {} != {}'.format(self.code202, self.code23, s22))
        self.assertEqual(s201, self.code201, '{} + {} != {}'.format(self.code202, self.code13, s22))
        self.assertEqual(s32, self.code32, '{} + {} != {}'.format(self.code120, self.code12, s22))
        self.assertEqual(s33, self.code33, '{} + {} != {}'.format(self.code103, self.code230, s22))

    def test_mul(self):
        m2131 = self.code21*self.code101
        c2131 = Code(c=[2,1,3,1])
        self.assertEqual(m2131, c2131, '{} * {} = {} != {}'.format(self.code21, self.code101, m2131, c2131))
        c3213 = Code(c=[3,2,1,3])
        m31201 = c3213*self.code12
        c31201 = Code(c=[3,1,2,0,1])
        self.assertEqual(m31201, c31201, '{} * {} = {} != {}'.format(c3213, self.code12, m31201, c31201))
        c10 = Code(c=[1,0])
        m230 = self.code23*c10
        self.assertEqual(m230, self.code230, '{} * {} = {} != {}'.format(self.code23, c10, m230, self.code230))
        
    def test_zero(self):
        s0 = Sequence(c=[0])
        c0 = Code(sequence=s0)
        self.assertEqual(c0,Code(c=[0]))
        
    def test_normalization_valid_codes(self):
        s0 = Sequence(c=[0])
        s110 = Sequence(c=[1,-1,0])
        s120 = Sequence(c=[1,-2,0])
        s130 = Sequence(c=[1,-3,0])
        s210 = Sequence(c=[2,-1,0])
        s220 = Sequence(c=[2,-2,0])
        s230 = Sequence(c=[2,-3,0])
        s310 = Sequence(c=[3,-1,0])
        s320 = Sequence(c=[3,-2,0])
        s330 = Sequence(c=[3,-3,0])
        s101 = Sequence(c=[1,0,-1])
        s102 = Sequence(c=[1,0,-2])
        s103 = Sequence(c=[1,0,-3])
        s201 = Sequence(c=[2,0,-1])
        s202 = Sequence(c=[2,0,-2])
        s203 = Sequence(c=[2,0,-3])
        s301 = Sequence(c=[3,0,-1])
        s302 = Sequence(c=[3,0,-2])
        s303 = Sequence(c=[3,0,-3])
        s11 = Sequence(c=[1,-1])
        s12 = Sequence(c=[1,-2])
        s13 = Sequence(c=[1,-3])
        s21 = Sequence(c=[2,-1])
        s22 = Sequence(c=[2,-2])
        s23 = Sequence(c=[2,-3])
        s31 = Sequence(c=[3,-1])
        s32 = Sequence(c=[3,-2])
        s33 = Sequence(c=[3,-3])
        c0 = Code(sequence=s0)
        c110 = Code(sequence=s110)
        c120 = Code(sequence=s120)
        c130 = Code(sequence=s130)
        c210 = Code(sequence=s210)
        c220 = Code(sequence=s220)
        c230 = Code(sequence=s230)
        c310 = Code(sequence=s310)
        c320 = Code(sequence=s320)
        c330 = Code(sequence=s330)
        c101 = Code(sequence=s101)
        c102 = Code(sequence=s102)
        c103 = Code(sequence=s103)
        c201 = Code(sequence=s201)
        c202 = Code(sequence=s202)
        c203 = Code(sequence=s203)
        c301 = Code(sequence=s301)
        c302 = Code(sequence=s302)
        c303 = Code(sequence=s303)
        c11 = Code(sequence=s11)
        c12 = Code(sequence=s12)
        c13 = Code(sequence=s13)
        c21 = Code(sequence=s21)
        c22 = Code(sequence=s22)
        c23 = Code(sequence=s23)
        c32 = Code(sequence=s31)
        c32 = Code(sequence=s32)
        c33 = Code(sequence=s33)
        self.assertTrue(np.array_equal(c0._legalCode, np.array([0])), 'error en la normalizacion de {}'.format(c0))