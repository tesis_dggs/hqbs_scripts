#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 16:07:31 2019

@author: federico
"""
import unittest
import sys
sys.path.append("../")
from snyder.snyder import ISEA, FullersDymaxion
from coordinates.coordinates import WGS84Coordinate, CartesianCoordinate
import numpy as np

class TestCase(unittest.TestCase):
    def setUp(self):
        self._ISEAProj = ISEA(ajuste=1e16)
        self._FDProj = FullersDymaxion(ajuste=1e16)
        self._centros = {
            1: WGS84Coordinate(longitude=-180.0,latitude=52.62263186),
            2: WGS84Coordinate(longitude=-108.0,latitude=52.62263186),
            3: WGS84Coordinate(longitude=-36.0,latitude=52.62263186),
            4: WGS84Coordinate(longitude=36.0,latitude=52.62263186),
            5: WGS84Coordinate(longitude=108.0,latitude=52.62263186),
            6: WGS84Coordinate(longitude=-180.0,latitude=10.81231696),
            7: WGS84Coordinate(longitude=-108.0,latitude=10.81231696),
            8: WGS84Coordinate(longitude=-36.0,latitude=10.81231696),
            9: WGS84Coordinate(longitude=36.0,latitude=10.81231696),
            10: WGS84Coordinate(longitude=108.0,latitude=10.81231696),
            11: WGS84Coordinate(longitude=-144.0,latitude=-10.81231696),
            12: WGS84Coordinate(longitude=-72.0,latitude=-10.81231696),
            13: WGS84Coordinate(longitude=0.0,latitude=-10.81231696),
            14: WGS84Coordinate(longitude=72.0,latitude=-10.81231696),
            15: WGS84Coordinate(longitude=144.0,latitude=-10.81231696),
            16: WGS84Coordinate(longitude=-144.0,latitude=-52.62263186),
            17: WGS84Coordinate(longitude=-72.0,latitude=-52.62263186),
            18: WGS84Coordinate(longitude=0.0,latitude=-52.62263186),
            19: WGS84Coordinate(longitude=72.0,latitude=-52.62263186),
            20: WGS84Coordinate(longitude=144.0,latitude=-52.62263186)
        }
        self._verticesFD = {
            'LIBERIA': WGS84Coordinate(longitude=-5.24539058,latitude=2.30088201),
            'NORWAY': WGS84Coordinate(longitude=10.53619898,latitude=64.70000000),
            'ARABIAN SEA': WGS84Coordinate(longitude=58.15770555,latitude=10.44734504),
            'BUENOS AIRES': WGS84Coordinate(longitude=-57.70000000,latitude=-39.10000000),
            'PUERTO RICO': WGS84Coordinate(longitude=-67.13232659,latitude=23.71792533),
            'CAPE TOWN': WGS84Coordinate(longitude=36.52150967,latitude=-50.10320164),
            'CHINA': WGS84Coordinate(longitude=122.30000000,latitude=39.10000000),
            'ALASKA': WGS84Coordinate(longitude=-143.47849033,latitude=50.10320164),
            'ANTARTICA': WGS84Coordinate(longitude=-169.46380102,latitude=-64.70000000),
            'PITCAIRN ISLAND': WGS84Coordinate(longitude=-121.84229445,latitude=-10.44734504),
            'AUSTRALIA': WGS84Coordinate(longitude=112.86767341,latitude=-23.71792533),
            'GILBERT ISLAND': WGS84Coordinate(longitude=174.75460942,latitude=-2.30088201)
        }
        self._puntosFD = {
            0: {
                'isea': 2,
                'fd': 13,
                'punto': WGS84Coordinate(longitude=-121.32, latitude=37.49)
            },
            1: {
                'isea': 3,
                'fd': 8,
                'punto': WGS84Coordinate(longitude=-23.70, latitude=71.18)
            },
            2: {
                'isea': 4,
                'fd': 7,
                'punto': WGS84Coordinate(longitude=42.41, latitude=64.97)
            },
            3: {
                'isea': 5,
                'fd': 12,
                'punto': WGS84Coordinate(longitude=124.23, latitude=52.12)
            },
            4: {
                'isea': 18,
                'fd': 1,
                'punto': WGS84Coordinate(longitude=24.39, latitude=-33.88)
            },
            5: {
                'isea': 12,
                'fd': 4,
                'punto': WGS84Coordinate(longitude=-56.10, latitude=-22.60)
            },
            6: {
                'isea': 14,
                'fd': 11,
                'punto': WGS84Coordinate(longitude=74.24, latitude=14.97)
            }
        }
    
    def test_ISEA(self):
        for k in self._centros.keys():
            (coord, cara) = self._ISEAProj.forwardProjection(coord=self._centros[k])
            self.assertEqual(cara, k, "cara {}, k {}".format(cara, k))
            self.assertLessEqual(coord.x, self._ISEAProj.getEpsilon(), "x {}".format(coord.x))
            self.assertGreaterEqual(coord.x, -self._ISEAProj.getEpsilon(), "x {}".format(coord.x))
            self.assertLessEqual(coord.y, self._ISEAProj.getEpsilon(), "x {}".format(coord.y))
            self.assertGreaterEqual(coord.y, -self._ISEAProj.getEpsilon(), "x {}".format(coord.y))
    
    def test_FullersDymaxionVertex(self):
        for k in self._verticesFD.keys():
            vertice = self._verticesFD[k]
            coord = self._FDProj._toFullersDymaxionCoords(coord=vertice)
            print("vertice: {}, coordenadas {}".format(k, coord))

    def test_FD(self):
        for k in self._puntosFD.keys():
            p = self._puntosFD[k]['punto']
            (coordISEA, caraISEA) = self._ISEAProj.forwardProjection(coord=p)
            (coordFD, caraFD) = self._FDProj.forwardProjection(coord=p)
            self.assertEqual(caraISEA, self._puntosFD[k]['isea'], "isea: {}, calculado: {}".format(self._puntosFD[k]['isea'], caraISEA))
            self.assertEqual(caraFD, self._puntosFD[k]['fd'], "fd: {}, calculado: {}".format(self._puntosFD[k]['fd'], caraFD))