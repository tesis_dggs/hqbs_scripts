#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 15 11:50:10 2019

@author: federico
"""
import numpy as np
import copy
from coordinates.coordinates import WGS84Coordinate, CartesianCoordinate

class ISEA:

    def __init__(self, ajuste):
        # epsilon de error de double
        self._EPSILON = 0.000005

        self._DEG36 = 0.62831853071795864768
        self._DEG72 = 1.25663706143591729537
        self._DEG90 = np.pi/2.0
        self._DEG108 = 1.88495559215387594306
        self._DEG120 = 2.09439510239319549229
        self._DEG144 = 2.51327412287183459075
        self._DEG180 = np.pi

        self._V_LAT = 0.46364760899944494524
        self._E_RAD = 0.91843818702186776133
        self._F_RAD = 0.18871053072122403508

        # self._icosahedronTriangles = {
        #     0: WGS84Coordinate(longitude=0.0,latitude=0.0),
        #     1: WGS84Coordinate(longitude=-self._DEG144,latitude=self._E_RAD),
        #     2: WGS84Coordinate(longitude=-self._DEG72,latitude=self._E_RAD),
        #     3: WGS84Coordinate(longitude=0.0,latitude=self._E_RAD),
        #     4: WGS84Coordinate(longitude=self._DEG72,latitude=self._E_RAD),
        #     5: WGS84Coordinate(longitude=self._DEG144,latitude=self._E_RAD),
        #     6: WGS84Coordinate(longitude=-self._DEG144,latitude=self._F_RAD),
        #     7: WGS84Coordinate(longitude=-self._DEG72,latitude=self._F_RAD),
        #     8: WGS84Coordinate(longitude=0.0,latitude=self._F_RAD),
        #     9: WGS84Coordinate(longitude=self._DEG72,latitude=self._F_RAD),
        #     10: WGS84Coordinate(longitude=self._DEG144,latitude=self._F_RAD),
        #     11: WGS84Coordinate(longitude=-self._DEG108,latitude=-self._F_RAD),
        #     12: WGS84Coordinate(longitude=-self._DEG36,latitude=-self._F_RAD),
        #     13: WGS84Coordinate(longitude=self._DEG36,latitude=-self._F_RAD),
        #     14: WGS84Coordinate(longitude=self._DEG108,latitude=-self._F_RAD),
        #     15: WGS84Coordinate(longitude=self._DEG180,latitude=-self._F_RAD),
        #     16: WGS84Coordinate(longitude=-self._DEG108,latitude=-self._E_RAD),
        #     17: WGS84Coordinate(longitude=-self._DEG36,latitude=-self._E_RAD),
        #     18: WGS84Coordinate(longitude=self._DEG36,latitude=-self._E_RAD),
        #     19: WGS84Coordinate(longitude=self._DEG108,latitude=-self._E_RAD),
        #     20: WGS84Coordinate(longitude=self._DEG180,latitude=-self._E_RAD)
        # }

        # self._vertex = {
        #     0: WGS84Coordinate(longitude=0.0,latitud=self._DEG90),
        #     1: WGS84Coordinate(longitude=self._DEG180,latitud=self._V_LAT),
        #     2: WGS84Coordinate(longitude=-self._DEG108,latitud=self._V_LAT),
        #     3: WGS84Coordinate(longitude=-self._DEG36,latitud=self._V_LAT),
        #     4: WGS84Coordinate(longitude=self._DEG36,latitud=self._V_LAT),
        #     5: WGS84Coordinate(longitude=self._DEG108,latitud=self._V_LAT),
        #     6: WGS84Coordinate(longitude=-self._DEG144,latitud=-self._V_LAT),
        #     7: WGS84Coordinate(longitude=-self._DEG72,latitud=-self._V_LAT),
        #     8: WGS84Coordinate(longitude=0.0,latitud=-self._V_LAT),
        #     9: WGS84Coordinate(longitude=self._DEG72,latitud=-self._V_LAT),
        #     10: WGS84Coordinate(longitude=self._DEG144,latitud=-self._V_LAT),
        #     11: WGS84Coordinate(longitude=0.0,latitud=-self._DEG90)
        # }

        self._icosahedronTriangles = {
            0: WGS84Coordinate(longitude=0.0,latitude=0.0),
            1: WGS84Coordinate(longitude=-self._DEG180,latitude=self._E_RAD),
            2: WGS84Coordinate(longitude=-self._DEG108,latitude=self._E_RAD),
            3: WGS84Coordinate(longitude=-self._DEG36,latitude=self._E_RAD),
            4: WGS84Coordinate(longitude=self._DEG36,latitude=self._E_RAD),
            5: WGS84Coordinate(longitude=self._DEG108,latitude=self._E_RAD),
            6: WGS84Coordinate(longitude=-self._DEG180,latitude=self._F_RAD),
            7: WGS84Coordinate(longitude=-self._DEG108,latitude=self._F_RAD),
            8: WGS84Coordinate(longitude=-self._DEG36,latitude=self._F_RAD),
            9: WGS84Coordinate(longitude=self._DEG36,latitude=self._F_RAD),
            10: WGS84Coordinate(longitude=self._DEG108,latitude=self._F_RAD),
            11: WGS84Coordinate(longitude=-self._DEG144,latitude=-self._F_RAD),
            12: WGS84Coordinate(longitude=-self._DEG72,latitude=-self._F_RAD),
            13: WGS84Coordinate(longitude=0.0,latitude=-self._F_RAD),
            14: WGS84Coordinate(longitude=self._DEG72,latitude=-self._F_RAD),
            15: WGS84Coordinate(longitude=self._DEG144,latitude=-self._F_RAD),
            16: WGS84Coordinate(longitude=-self._DEG144,latitude=-self._E_RAD),
            17: WGS84Coordinate(longitude=-self._DEG72,latitude=-self._E_RAD),
            18: WGS84Coordinate(longitude=0.0,latitude=-self._E_RAD),
            19: WGS84Coordinate(longitude=self._DEG72,latitude=-self._E_RAD),
            20: WGS84Coordinate(longitude=self._DEG144,latitude=-self._E_RAD)
        }

        self._vertex = {
            0: WGS84Coordinate(longitude=0.0,latitude=self._DEG90),
            1: WGS84Coordinate(longitude=-self._DEG144,latitude=self._V_LAT),
            2: WGS84Coordinate(longitude=-self._DEG72,latitude=self._V_LAT),
            3: WGS84Coordinate(longitude=0.0,latitude=self._V_LAT),
            4: WGS84Coordinate(longitude=self._DEG72,latitude=self._V_LAT),
            5: WGS84Coordinate(longitude=self._DEG144,latitude=self._V_LAT),
            6: WGS84Coordinate(longitude=-self._DEG180,latitude=-self._V_LAT),
            7: WGS84Coordinate(longitude=-self._DEG108,latitude=-self._V_LAT),
            8: WGS84Coordinate(longitude=-self._DEG36,latitude=-self._V_LAT),
            9: WGS84Coordinate(longitude=self._DEG36,latitude=-self._V_LAT),
            10: WGS84Coordinate(longitude=self._DEG108,latitude=-self._V_LAT),
            11: WGS84Coordinate(longitude=0.0,latitude=-self._DEG90)
        }

        self._tri_v1 = [0, 0, 0, 0, 0, 0, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 11, 11, 11, 11, 11]

        self._g = 37.37736814 * np.pi / 180.0
        self._G = 36.0 * np.pi / 180.0
        self._theta = 30.0 * np.pi / 180.0

        self._factorAjuste = ajuste

    def sphAzimuth(self, f_lon, f_lat, t_lon, t_lat):
        az = np.arctan2(np.cos(t_lat) * np.sin(t_lon - f_lon), np.cos(f_lat) * np.sin(t_lat) - np.sin(f_lat) * np.cos(t_lat) * np.cos(t_lon - f_lon))
        return az

    def azAdjustment(self, triangle):
        v = self._vertex[self._tri_v1[triangle]]
        c = self._icosahedronTriangles[triangle]

        # TODO looks like the adjustment is always either 0 or 180
        # at least if you pick your vertex carefully
        adj = np.arctan2(np.cos(v.lat) * np.sin(v.lon - c.lon), np.cos(c.lat) * np.sin(v.lat) - np.sin(c.lat) * np.cos(v.lat) * np.cos(v.lon - c.lon))
        return adj

    def forwardProjection(self, coord):
        lat = coord.lat * np.pi / 180.0
        lon = coord.lon * np.pi / 180.0
        for i in range(1,len(self._icosahedronTriangles) + 1):
            center = self._icosahedronTriangles[i]

            # step 1

            z = np.arccos(np.sin(center.lat) * np.sin(lat) + np.cos(center.lat) * np.cos(lat) * np.cos(lon - center.lon))
            # not on this triangle 
            if (z > self._g + self._EPSILON):
                continue

            Az = self.sphAzimuth(f_lon=center.lon, f_lat=center.lat, t_lon=lon, t_lat=lat)
            
            # step 2

            # This calculates "some" vertex coordinate
            az_offset = self.azAdjustment(triangle=i)

            Az -= az_offset

            # TODO I don't know why we do this.  It's not in snyder
            # maybe because we should have picked a better vertex
            if (Az < 0.0):
                Az += 2.0 * np.pi

            # adjust Az for the point to fall within the range of 0 to
            # 2(90 - theta) or 60 degrees for the hexagon, by
            # and therefore 120 degrees for the triangle
            # of the icosahedron
            # subtracting or adding multiples of 60 degrees to Az and
            # recording the amount of adjustment

            Az_adjust_multiples = 0
            while (Az < 0.0):
                Az += self._DEG120
                Az_adjust_multiples -= 1
            while (Az > self._DEG120 + self._EPSILON): # TODO DBL_EPSILON):
                Az -= self._DEG120
                Az_adjust_multiples += 1
            
            # step 3

            cot_theta = 1.0 / np.tan(self._theta)
            tan_g = np.tan(self._g) # TODO this is a constant

            # Calculate q from eq 9.
            # TODO cot_theta is cot(30)
            q = np.arctan2(tan_g, np.cos(Az) + np.sin(Az) * cot_theta)

            # not in this triangle
            if (z > q + self._EPSILON):
                continue

            # step 4

            # Apply equations 5-8 and 10-12 in order

            # eq 5
            # Rprime = 0.9449322893 * R;
            # R' in the paper is for the truncated
            Rprime = 0.91038328153090290025

            # eq 6
            H = np.arccos(np.sin(Az) * np.sin(self._G) * np.cos(self._g) - np.cos(Az) * np.cos(self._G))

            # eq 7
            # Ag = (Az + G + H - DEG180) * M_PI * R * R / DEG180;
            Ag = Az + self._G + H - self._DEG180

            # eq 8
            Azprime = np.arctan2(2.0 * Ag, Rprime * Rprime * tan_g * tan_g - 2.0 * Ag * cot_theta)

            # eq 10
            # cot(theta) = 1.73205080756887729355
            dprime = Rprime * tan_g / (np.cos(Azprime) + np.sin(Azprime) * cot_theta)

            # eq 11
            f = dprime / (2.0 * Rprime * np.sin(q / 2.0))

            # eq 12
            rho = 2.0 * Rprime * f * np.sin(z / 2.0)

            #
            # add back the same 60 degree multiple adjustment from step
            # 2 to Azprime
            #

            Azprime += self._DEG120 * Az_adjust_multiples

            # calculate rectangular coordinates 

            x = rho * np.sin(Azprime) * self._factorAjuste
            y = rho * np.cos(Azprime) * self._factorAjuste

            #
            # TODO
            # translate coordinates to the origin for the particular
            # hexagon on the flattened polyhedral map plot
            #

            return (CartesianCoordinate(x=x, y=y), i)

    def forwardProjectionFace(self, coord, face):
        lat = coord.lat * np.pi / 180.0
        lon = coord.lon * np.pi / 180.0
        center = self._icosahedronTriangles[face]

        # step 1

        z = np.arccos(np.sin(center.lat) * np.sin(lat) + np.cos(center.lat) * np.cos(lat) * np.cos(lon - center.lon))
        # not on this triangle 
        if (z > self._g + self._EPSILON):
            print('impossible transform: {} is not on any triangle'.format(coord))

            return None # suppresses a warning

        Az = self.sphAzimuth(f_lon=center.lon, f_lat=center.lat, t_lon=lon, t_lat=lat)
        
        # step 2

        # This calculates "some" vertex coordinate
        az_offset = self.azAdjustment(triangle=face)

        Az -= az_offset

        # TODO I don't know why we do this.  It's not in snyder
        # maybe because we should have picked a better vertex
        if (Az < 0.0):
            Az += 2.0 * np.pi

        # adjust Az for the point to fall within the range of 0 to
        # 2(90 - theta) or 60 degrees for the hexagon, by
        # and therefore 120 degrees for the triangle
        # of the icosahedron
        # subtracting or adding multiples of 60 degrees to Az and
        # recording the amount of adjustment

        Az_adjust_multiples = 0
        while (Az < 0.0):
            Az += self._DEG120
            Az_adjust_multiples -= 1
        while (Az > self._DEG120 + self._EPSILON):
            Az -= self._DEG120
            Az_adjust_multiples += 1
        
        # step 3

        cot_theta = 1.0 / np.tan(self._theta)
        tan_g = np.tan(self._g) # TODO this is a constant

        # Calculate q from eq 9.
        # TODO cot_theta is cot(30)
        q = np.arctan2(tan_g, np.cos(Az) + np.sin(Az) * cot_theta)

        # not in this triangle
        if (z > q + self._EPSILON):
            print('impossible transform: {} is not on any triangle'.format(coord))

            return None # suppresses a warning

        # step 4

        # Apply equations 5-8 and 10-12 in order

        # eq 5
        # Rprime = 0.9449322893 * R;
        # R' in the paper is for the truncated
        Rprime = 0.91038328153090290025

        # eq 6
        H = np.arccos(np.sin(Az) * np.sin(self._G) * np.cos(self._g) - np.cos(Az) * np.cos(self._G))

        # eq 7
        # Ag = (Az + G + H - DEG180) * M_PI * R * R / DEG180;
        Ag = Az + self._G + H - self._DEG180

        # eq 8
        Azprime = np.arctan2(2.0 * Ag, Rprime * Rprime * tan_g * tan_g - 2.0 * Ag * cot_theta)

        # eq 10
        # cot(theta) = 1.73205080756887729355
        dprime = Rprime * tan_g / (np.cos(Azprime) + np.sin(Azprime) * cot_theta)

        # eq 11
        f = dprime / (2.0 * Rprime * np.sin(q / 2.0))

        # eq 12
        rho = 2.0 * Rprime * f * np.sin(z / 2.0)

        #
        # add back the same 60 degree multiple adjustment from step
        # 2 to Azprime
        #

        Azprime += self._DEG120 * Az_adjust_multiples

        # calculate rectangular coordinates 

        x = rho * np.sin(Azprime)
        y = rho * np.cos(Azprime)

        #
        # TODO
        # translate coordinates to the origin for the particular
        # hexagon on the flattened polyhedral map plot
        #

        return CartesianCoordinate(x=x, y=y)
    
    def getEpsilon(self):
        return self._EPSILON

class FullersDymaxion(ISEA):

    def __init__(self, ajuste):
        super().__init__(ajuste=ajuste)
        self._northPole = WGS84Coordinate(longitude=-5.24539058, latitude=2.30088201) #(longitude=10.53619898, latitude=64.70000000)

    def _toFullersDymaxionCoords(self, coord):
        oldWGS84 = copy.deepcopy(coord)
        oldWGS84.lon += 180.0

        phi = oldWGS84.lat * np.pi / 180.0
        lam = oldWGS84.lon * np.pi / 180.0
        alpha = self._northPole.lat * np.pi / 180.0
        beta = self._northPole.lon * np.pi / 180.0
        lam0 = beta

        cos_p = np.cos(phi)
        sin_a = np.sin(alpha)

        # mpawm 5-7
        sin_phip = sin_a * np.sin(phi) - np.cos(alpha) * cos_p * np.cos(lam - lam0)

        # mpawm 5-8b

        # use the two argument form so we end up in the right quadrant
        lp_b = np.arctan2(cos_p * np.sin(lam - lam0), (sin_a * cos_p * np.cos(lam - lam0) + np.cos(alpha) * np.sin(phi)))

        lamp = lp_b + beta

        # normalize longitude
        # TODO can we just do a modulus ?
        lamp = lamp % (2.0 * np.pi)
        while lamp > np.pi:
            lamp -= 2.0 * np.pi
        while lamp < -np.pi:
            lamp += 2.0 * np.pi
        
        phip = np.arcsin(sin_phip)

        newWGS84 = WGS84Coordinate(longitude=lamp * 180.0 / np.pi, latitude=phip * 180.0 / np.pi)

        #newWGS84.lon -= (180.0 - 7.46658 + coord.lon)
        newWGS84.lat = np.round(a=newWGS84.lat, decimals=7)
        newWGS84.lon = np.round(a=newWGS84.lon, decimals=7)
        newWGS84.lon += (5.2453906 + 90 + 25.4665837)
        newWGS84.lon += 180.0
        # normalize longitude
        newWGS84.lon = newWGS84.lon % 360.0
        while newWGS84.lon > 180.0:
            newWGS84.lon -= 360.0
        while newWGS84.lon < -180.0:
            newWGS84.lon += 360.0

        return newWGS84

    def forwardProjection(self, coord):
        newWGS84 = self._toFullersDymaxionCoords(coord=coord)
        return super().forwardProjection(coord=newWGS84)